#version 430

#define ONE_OVER_4PI	0.0795774715459476

#define BLEND_START		0	
#define BLEND_END		0

out vec4 my_FragColor0;

layout(binding = 1) uniform sampler2D perlin;
layout(binding = 2) uniform samplerCube envmap;

uniform vec4 uvParams;
uniform vec2 perlinOffset;
uniform vec3 oceanColor;

in vec3 vertexDir;
in vec2 tex;  // uvCoordinates

const vec3 sunColor			= vec3(1.0, 1.0, 0.47);
const vec3 perlinGradient	= vec3(0.012, 0.016, 0.022);
const vec3 sundir			= vec3(0.603, 0.240, -0.761);

void main()
{
	// blend Perlin waves
	vec2 p0 = texture(perlin, tex * 1.12 + perlinOffset).rg;
	vec2 p1 = texture(perlin, tex * 0.23 + perlinOffset).rg;

	// calculate reflections
	vec3 grad = vec3(0, 0.20, 0);
	grad.xz = (p0 * perlinGradient.x + p1 * perlinGradient.z) * 2;

	vec3 n = normalize(grad.xyz);
	vec3 v = normalize(vertexDir);
	vec3 l = reflect(-v, n);

	float F0 = 0.5; // reflection factor
	float F = F0 + (1.0 - F0) * pow(1.0 - dot(n, l), 5.0); // env, ocean blend factor

	vec3 env = texture(envmap, l).rgb; // env reflecion

	// Ward model - sun spectacular
	const float rho = 0.3;
	const float ax = 0.35;
	const float ay = 0.2;

	vec3 h = sundir + v;
	vec3 x = cross(sundir, n);
	vec3 y = cross(x, n);

	float mult = (ONE_OVER_4PI * rho / (ax * ay * sqrt(max(1e-5, dot(sundir, n) * dot(v, n)))));
	float hdotx = dot(h, x) / ax;
	float hdoty = dot(h, y) / ay;
	float hdotn = dot(h, n);

	float spec = mult * exp(-((hdotx * hdotx) + (hdoty * hdoty)) / (hdotn * hdotn));
	
	my_FragColor0 = vec4(mix(oceanColor, env, F) + sunColor * spec, 1.0);
}
