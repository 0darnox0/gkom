#version 430

layout (location = 0) in vec3 vertexPos;

uniform mat4 scale;
uniform mat4 matViewProj;
uniform vec4 uvParams;
uniform vec3 eyePos;

out vec3 vertexDir;
out vec2 tex;

void main()
{
	vec4 posScaled = scale * vec4(vertexPos, 1.0);
	vec2 uvCoordinates = posScaled.xz * uvParams.x;

	vertexDir = eyePos - posScaled.xyz;
	tex = uvCoordinates;

	gl_Position = matViewProj * vec4(posScaled.xyz, 1.0);
}
