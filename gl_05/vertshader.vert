#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec2 texCoord;
layout (location = 3) in vec3 normal;

out vec3 vecColor;
out vec2 TexCoord;
out vec3 Position_worldspace;
out vec3 Normal_cameraspace;
out vec3 EyeDirection_cameraspace;
out vec3 LightDirection_cameraspace;
  
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 LightPosition_worldspace;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0f);
	Position_worldspace = (model * vec4(position,1)).xyz;

	vec3 vertexPosition_cameraspace = ( view * model * vec4(position,1)).xyz;
	EyeDirection_cameraspace = vec3(0,0,0) - vertexPosition_cameraspace;

	vec3 LightPosition_cameraspace = ( view * vec4(LightPosition_worldspace,1)).xyz;
	LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;

	//Normal_cameraspace = (view * model * vec4(normal, 0.0)).xyz; 
	Normal_cameraspace = mat3(transpose(inverse(model))) * normal;

    vecColor = color;
    TexCoord = texCoord;
} 