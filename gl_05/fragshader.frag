#version 330 core
in vec2 TexCoord;
in vec3 vecColor;
in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;

out vec3 color;

uniform sampler2D Texture;
uniform mat4 MV;
uniform vec3 LightPosition_worldspace;

void main()
{

	vec3 LightColor = vec3(1,1,1);
	float LightPower = 55000.0f;

	vec3 MaterialDiffuseColor = vecColor; //texture( Texture, TexCoord ).rgb;
	//vec3 MaterialAmbientColor = 0,1 * LightColor * MaterialDiffuseColor;
	vec3 MaterialAmbientColor = vec3(0.1,0.1,0.1) * MaterialDiffuseColor;
	vec3 MaterialSpecularColor = vec3(0.1,0.1,0.);

	float distance = length( LightPosition_worldspace - Position_worldspace );

	vec3 n = normalize( Normal_cameraspace );
	//vec3 l = normalize( LightDirection_cameraspace );
	vec3 l = normalize( LightPosition_worldspace - Position_worldspace );
	float cosTheta = clamp( dot( n,l ), 1, 1 );
	//float cosTheta = max( dot( n,l ), 0.0 );

	vec3 E = normalize(EyeDirection_cameraspace);
	vec3 R = reflect(-l,n);
	//float cosAlpha = clamp( dot( E,R ), 0,1 );
	float cosAlpha = max( dot( E,R ), 0.0 );

	color = 
		// Ambient
		MaterialAmbientColor +
		// Diffuse
		MaterialDiffuseColor * LightColor * LightPower * cosTheta / (distance*distance) +
		// Specular
		MaterialSpecularColor * LightColor * LightPower * pow(cosAlpha,32) / (distance*distance);

}
